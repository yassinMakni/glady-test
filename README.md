Remarques: 
 - chaque beneficiaire est crée avec une wallet sans cartes 
 - chaque wallet peut contenir 2 cartes: carte GIFT et carte MEAL 

afin de tester au lancement de l'application il y a un fichier data.sql executé
il ya 3 api destiné au entreprise 
- api pour distribuer un gift
  http://localhost:8080/company/distribute/gift POST
  exemple de requete
  {
  "userId" : 1,
  "amount" : 40
  }
- api pour distribuer un meal
  http://localhost:8080/company/distribute/meal POST
  exemple de requete 
  {
  "userId" : 1,
  "amount" : 40
  }

  - api pour calculer le solde d'un beneficiaire
    http://localhost:8080/company/user/balance/{userId} GET
    exemple de reponse :
    {
    "cards": [
    {
    "cardId": 1,
    "cardType": "GIFT",
    "cardBalance": 40,
    "notExpiredBalance": 40.0
    },
    {
      FeadBack client glady :
      Bonjour Yassin, 

Bonne année. Avec les congés de chacun, je n'ai pu te répondre plus tôt mais tu trouveras ci-dessous d'autres éléments de réponses par le Head of Engineering concernant ton travail, bonne lecture.

"Le readme n'est pas très clair, non relu ou dans un français parfois incorrect. Si je veux lancer l'application c'est à moi de trouver comment démarrer l'appli et la base de données : le but n'est pas de faire un jeu de piste mais d'expliquer le projet. Ici, il faut d'ailleurs que j'identifie moi même quelle base de données est utilisée;
Un dockerfile pour les dépendances est souvent apprécié;
Le nommage n'est pas standardisé ni dans les packages ni même dans les attributs de classe : parfois au singulier, parfois au pluriel, parfois en minuscule ou en majuscule;
Ça ressemble plutôt à une architecture trois tiers mais les services sont des interfaces d'une ou deux méthodes (voire même 0), ce qui ne fait pas beaucoup de sens à mes yeux ou alors c'est le genre de choix qu'il aurait fallu préciser dans le readme en expliquant pourquoi. Quitte à mettre des interfaces, ç'aurait été plus intéressant d'en implémenter sur les repositories ou les controllers pour montrer l'ébauche d'une clean architecture par exemple;
Les one-liners de builders avec plus de 100 caractères;
L'ApiResponse mélangée avec les exceptions et qui n'est pas utilisée dans les controllers, ni étendue par les types Response custom - je pense d'ailleurs que ce n'est pas lui qui a écrit les classes du répertoire d'exception, c'aurait été bien de standardiser les commentaires dans tous les cas;
Des concaténations de string en dur (ex: "01-"+"02" ?);
Les loggers spécifiés dans chaque classe mais avec une visibilité default et non statiques. On recommande en général de les mettre en private et statiques à la fois par efficience et sécurité;
Un seul controller (company) qui traite tous les sujets même pour les users alors que les services sont mieux découpés;
Code non linté.
Quelques point positifs tout de même:

Il a des bases de springboot;
Il a fait des tests qui sont très bien, dommage que l'application en elle même ne soit pas du même niveau;
Il connait quelques librairies utiles comme Lombok;
Même si l'utilisation n'est pas poussée, il a l'air de connaitre les bases de JPA.
En conclusion, il sait faire des choses mais on dirait que certains choix architecturaux sont mal maitrisés par rapport aux autres candidats avec une expérience similaire. Sur le plan technique, malgré de bons tests, ça mériterait plus d'application dans les détails sans pour autant prendre plus de temps dans la réalisation, supprimer les classes non utilisées ou linter le code n'est qu'un raccourci clavier à exécuter par exemple. Dernier élément, la documentation et la lisibilité ne semblent pas être des points d'attention pour lui, ce qui est dommage pour des applications d'un niveau professionnel."

Belle continuation,

Bien à toi, 

Valentine
    "cardId": 2,
    "cardType": "MEAL",
    "cardBalance": 40,
    "notExpiredBalance": 40.0
    }
    ]
    }
  TODO
  - la partie authetification et securité 
  - les apis pour créer des utilisateur avec wallet 
  - les apis pour créer des cartes GIFT et MEAL 
