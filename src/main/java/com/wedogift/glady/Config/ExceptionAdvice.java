package com.wedogift.glady.Config;

import com.wedogift.glady.Enum.ExceptionType;
import com.wedogift.glady.Model.Message.Exception.ApiException;
import com.wedogift.glady.Model.Message.Exception.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
@ControllerAdvice
@RestController
public class ExceptionAdvice {
    private final Logger LOG = LoggerFactory.getLogger(ExceptionAdvice.class);

    /**
     * Process bad request exception response entity.
     *
     * @param req the req
     * @param ex  the ex
     * @return the response entity
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiResponse> processBadRequestException(HttpServletRequest req, AccessDeniedException ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ExceptionType.UNAUTHORIZED.getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(null, false, ExceptionType.UNAUTHORIZED.name());
        return new ResponseEntity(apiResponse, httpStatus);
    }

    /**
     * Process server exception response entity.
     *
     * @param req the req
     * @param ex  the ex
     * @return the response entity
     */
    @ExceptionHandler()
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiResponse> processServerException(HttpServletRequest req, Exception ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ExceptionType.NOT_FOUND.getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(null, false, ExceptionType.USER_NOT_FOUND.name());
        return new ResponseEntity(apiResponse, httpStatus);
    }

    /**
     * Process server exception response entity.
     *
     * @param req the req
     * @param ex  the ex
     * @return the response entity
     */
    @ExceptionHandler()
    public ResponseEntity<ApiResponse> processServerException(HttpServletRequest req, RuntimeException ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ExceptionType.BAD_REQUEST.getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(null, false, ExceptionType.BAD_REQUEST.name());
        LOG.error(ex.getMessage(),ex);
        return new ResponseEntity(apiResponse, httpStatus);
    }

    /**
     * Process server exception response entity.
     *
     * @param req the req
     * @param ex  the ex
     * @return the response entity
     */
    @ExceptionHandler()
    public ResponseEntity<ApiResponse> processServerException(HttpServletRequest req, ApiException ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ex.getExceptionType().getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(ex.getMessage(), false, ex.getExceptionType().name());
        return new ResponseEntity(apiResponse, httpStatus);
    }

    @ExceptionHandler()
    public ResponseEntity<ApiResponse> processServerException(HttpServletRequest req, NoHandlerFoundException ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ExceptionType.NOT_FOUND.getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(null, false, ExceptionType.NOT_FOUND.name());
        return new ResponseEntity(apiResponse, httpStatus);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiResponse> processUnsupportedMediaType(HttpServletRequest req, HttpMediaTypeNotSupportedException ex) {
        HttpStatus httpStatus = HttpStatus.valueOf(ExceptionType.BAD_REQUEST.getExceptionCode());
        ApiResponse apiResponse = new ApiResponse(null, false, ExceptionType.BAD_REQUEST.name());
        return new ResponseEntity(apiResponse, httpStatus);
    }

}
