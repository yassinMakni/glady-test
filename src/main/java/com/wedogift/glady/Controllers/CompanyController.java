package com.wedogift.glady.Controllers;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Model.Message.DistributeRequest;
import com.wedogift.glady.Model.Message.UserBalanceResponse;
import com.wedogift.glady.Services.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/company")
public class CompanyController {
    Logger logger = LoggerFactory.getLogger(CompanyController.class);

    @Autowired
    private DistributionService distributionService;

    @PostMapping(value = "/distribute/gift", produces = MediaType.APPLICATION_JSON_VALUE)
    public void distributeGift(@RequestBody DistributeRequest request ) throws Exception{
        logger.trace("distributeGift - start");
         distributionService.distribute(request.getUserId(),request.getAmount(), CardType.GIFT);
        logger.trace("distributeGift - end");
    }
    @PostMapping(value = "/distribute/meal", produces = MediaType.APPLICATION_JSON_VALUE)
    public void distributeMeal(@RequestBody DistributeRequest request ) throws Exception{
        logger.trace("distributeMeal - start");
        distributionService.distribute(request.getUserId(),request.getAmount(),CardType.MEAL);
        logger.trace("distributeMeal - end");
    }
    @GetMapping(value = "/user/balance/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserBalanceResponse getUserBalance(@PathVariable final Long userId) throws Exception{
        logger.trace("getUserBalance - start");
       UserBalanceResponse response =  distributionService.getUserBalance(userId);
        logger.trace("getUserBalance - end");
        return response ;
    }

}
