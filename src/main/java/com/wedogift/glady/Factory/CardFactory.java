package com.wedogift.glady.Factory;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Model.Entities.Card;

public class CardFactory {

    public static Card createCard(CardType type,Long amount){
        Card card ;
        switch (type){
            case GIFT : card = Card.builder().balance(0L).type(CardType.GIFT).balance(amount).build();
            break;
            case MEAL : card = Card.builder().balance(0L).type(CardType.MEAL).balance(amount).build();
            break;
            default : throw new IllegalArgumentException("No such card with type: "+ type);
        }
        return card;
    }
}
