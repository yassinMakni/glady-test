package com.wedogift.glady.Factory;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Model.Entities.Card;
import com.wedogift.glady.Model.Entities.Distribution;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DistributionFactory {
    public static Distribution createDistribution(Card card, DistributionType type, Long amount) throws ParseException {
        Distribution distribution;
        switch (type) {
            case DEPOSIT:
                distribution = Distribution.builder().amount(amount).card(card).type(DistributionType.DEPOSIT).creationDate(new Date()).expiredDate(
                        CardType.GIFT.name().equals(card.getType().name()) ? getExpiredDateForGift() : getExpiredDateForMeal()
                ).build();
                break;
            case DEBIT:
                distribution = Distribution.builder().amount(amount).card(card).type(DistributionType.DEBIT).creationDate(new Date()).build();
                break;
            default:
                throw new IllegalArgumentException("No such distribution with type: " + type);
        }
        return distribution;
    }

    public static Date getExpiredDateForGift() {
        Date currentDate = new Date();
        // convert date to calendar
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.YEAR, 1);
        return c.getTime();
    }

    public static Date getExpiredDateForMeal() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        // convert date to calendar
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR) + 1;
        c.setTime(formatter.parse(year + "-02-" + "01"));
        return formatter.parse(year + "-02-" + c.getActualMaximum(Calendar.DAY_OF_MONTH));
    }
}
