package com.wedogift.glady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GladyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GladyApplication.class, args);
	}
}
