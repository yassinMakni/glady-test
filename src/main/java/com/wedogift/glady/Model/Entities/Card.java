package com.wedogift.glady.Model.Entities;

import com.wedogift.glady.Enum.CardType;
import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Entity
@Builder
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private CardType type ;

    private Long balance ;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Distribution> distributions ;

    public Card(){}

    public Card(Long id, CardType type, Long balance, List<Distribution> distributions) {
        this.id = id;
        this.type = type;
        this.balance = balance;
        this.distributions = distributions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }

    public void setDistributions(List<Distribution> distributions) {
        this.distributions = distributions;
    }
}
