package com.wedogift.glady.Model.Entities;


import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    private String Name;

    private Long balance ;

    public Company() {
    }

    public Company(Long id, String name, Long balance) {
        this.id = id;
        Name = name;
        this.balance = balance;
    }
}

