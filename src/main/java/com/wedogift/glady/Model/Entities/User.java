package com.wedogift.glady.Model.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "_user")
@Builder
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name ;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Company company;

    @OneToOne
    private Wallet wallet;

    public User(){}

    public User(Long id, String name, Company company, Wallet wallet) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.wallet = wallet;
    }
}
