package com.wedogift.glady.Model.Entities;


import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Builder
@Getter
@Setter
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Card> cards ;

    @OneToOne
    private User user;


    public Wallet(){

    }

    public Wallet(Long id, List<Card> cards, User user) {
        this.id = id;
        this.cards = cards;
        this.user = user;
    }
}
