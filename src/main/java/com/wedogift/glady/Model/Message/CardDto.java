package com.wedogift.glady.Model.Message;

import com.wedogift.glady.Enum.CardType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class CardDto {
    private Long cardId;
    private CardType cardType;
    private Long cardBalance;
    private Float notExpiredBalance ;
}
