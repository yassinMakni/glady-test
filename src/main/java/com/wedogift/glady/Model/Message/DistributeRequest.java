package com.wedogift.glady.Model.Message;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DistributeRequest {
    private Long userId;
    private Long amount;
}
