package com.wedogift.glady.Model.Message.Exception;

import com.wedogift.glady.Enum.ExceptionType;

public class ApiException extends RuntimeException{
    private ExceptionType exceptionType;
    /**
     * Instantiates a new Api exception.
     *
     * @param message       the message
     * @param exceptionType the exception type
     */
    public ApiException(String message, ExceptionType exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }
    /**
     * Gets exception type.
     *
     * @return the exception type
     */
    public ExceptionType getExceptionType() {
        return exceptionType;
    }
}
