package com.wedogift.glady.Model.Message.Exception;


import java.io.Serializable;

/**
 * The type Api response.
 *
 * @param <T> the type parameter
 */
public class ApiResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private T data;
    private Boolean success = Boolean.TRUE;
    private String error;

    /**
     * Instantiates a new Api response.
     */
    public ApiResponse() {
    }

    /**
     * Instantiates a new Api response.
     *
     * @param data    the data
     * @param success the success
     * @param error   the error
     */
    public ApiResponse(T data, Boolean success, String error) {
        this.data = data;
        this.success = success;
        this.error = error;
    }

    /**
     * Instantiates a new Api response.
     *
     * @param data the data
     */
    public ApiResponse(T data) {
        this.data = data;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public T getData() {
        return data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * Gets success.
     *
     * @return the success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * Sets success.
     *
     * @param success the success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Sets error.
     *
     * @param error the error
     */
    public void setError(String error) {
        this.error = error;
    }
}

