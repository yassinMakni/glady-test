package com.wedogift.glady.Model.Message.Exception;

public class ErrorMessage {
    public static final String USER_NOT_FOUND = "User not found with id %s";
    public static final String NOT_VALID_VALUE = "Not valid value for %s";
    public static final String COMPANY_NOT_FOUND = "Company not found with id %s";
    public static final String BALANCE_NOT_ENOUGH = "Company balance is not enough for this distribution";
    public static final String CARD_NOT_FOUND = "Card not found";
    public static final String PROBLEM_TECHNIQUE = "Technical Error";
}
