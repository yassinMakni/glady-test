package com.wedogift.glady.Model.Message;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Setter
@Getter
public class UserBalanceResponse {
   private List<CardDto> cards ;

}
