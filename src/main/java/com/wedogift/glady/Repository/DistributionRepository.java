package com.wedogift.glady.Repository;

import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Model.Entities.Distribution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DistributionRepository extends JpaRepository<Distribution,Long> {

    @Query(value = "select sum(d.amount) as total, d.card_id from distribution d " +
            "where d.type = ?2 " +
            "and d.expired_date > CURRENT_TIMESTAMP() " +
            "group by d.card_id HAVING d.card_id = ?1" , nativeQuery = true)
    Float getSumAllDepositDistributionNotExpired(Long cardId, String deposit);
}
