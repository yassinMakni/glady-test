package com.wedogift.glady.Services;

import com.wedogift.glady.Model.Entities.Card;

public interface CardService {
    public Card save(Card card);
}
