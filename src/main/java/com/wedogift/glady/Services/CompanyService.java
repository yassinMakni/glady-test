package com.wedogift.glady.Services;

import com.wedogift.glady.Model.Entities.Company;

import java.util.Optional;

public interface CompanyService {
    Optional<Company> findCompanyById(Long companyId);
    Company save(Company company);
}
