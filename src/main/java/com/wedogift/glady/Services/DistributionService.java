package com.wedogift.glady.Services;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Model.Entities.Distribution;
import com.wedogift.glady.Model.Message.UserBalanceResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DistributionService {

    public void distribute(Long userId, Long amount, CardType cardType) throws Exception;
    public UserBalanceResponse getUserBalance(Long userId) ;

}
