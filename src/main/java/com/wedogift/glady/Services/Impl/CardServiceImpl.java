package com.wedogift.glady.Services.Impl;

import com.wedogift.glady.Model.Entities.Card;
import com.wedogift.glady.Repository.CardRepository;
import com.wedogift.glady.Services.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private CardRepository cardRepository;

    @Override
    public Card save(Card card) {
        return cardRepository.save(card);
    }
}
