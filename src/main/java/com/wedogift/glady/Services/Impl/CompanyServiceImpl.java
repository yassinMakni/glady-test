package com.wedogift.glady.Services.Impl;

import com.wedogift.glady.Model.Entities.Company;
import com.wedogift.glady.Repository.CompanyRepository;
import com.wedogift.glady.Services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Optional<Company> findCompanyById(Long companyId) {
        //TODO get company from cache
        return companyRepository.findById(companyId);
    }

    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }
}
