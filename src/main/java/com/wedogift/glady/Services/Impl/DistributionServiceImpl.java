package com.wedogift.glady.Services.Impl;

import com.wedogift.glady.Controllers.CompanyController;
import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Enum.ExceptionType;
import com.wedogift.glady.Factory.DistributionFactory;
import com.wedogift.glady.Model.Entities.*;
import com.wedogift.glady.Model.Message.CardDto;
import com.wedogift.glady.Model.Message.Exception.ApiException;
import com.wedogift.glady.Model.Message.Exception.ErrorMessage;
import com.wedogift.glady.Model.Message.UserBalanceResponse;
import com.wedogift.glady.Repository.DistributionRepository;
import com.wedogift.glady.Services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DistributionServiceImpl implements DistributionService {

    @Autowired
    private UserService userService;
    @Autowired
    private CompanyService companyService;

    @Autowired
    private WalletService walletService;

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private CardService cardService;

    Logger logger = LoggerFactory.getLogger(DistributionServiceImpl.class);

    @Override
    public void distribute(Long userId, Long amount, CardType cardType) throws Exception {
        // TODO get company id form context
        // par defaut le user est creé avec une wallet sans cartes
        Long companyId = 1L;
        if (amount <= 0L) {
            logger.error(String.format(ErrorMessage.NOT_VALID_VALUE, "amount"));
            throw new ApiException(String.format(ErrorMessage.NOT_VALID_VALUE, "amount"), ExceptionType.BAD_REQUEST);
        }
        User user = userService.findUserById(userId).orElseThrow(() -> new ApiException(String.format(ErrorMessage.USER_NOT_FOUND, userId), ExceptionType.USER_NOT_FOUND));

        Company company = companyService.findCompanyById(companyId).orElseThrow(() -> new ApiException(String.format(ErrorMessage.COMPANY_NOT_FOUND, companyId), ExceptionType.USER_NOT_FOUND));
        if (user.getCompany().getId() != companyId) {
            logger.error(String.format(ErrorMessage.USER_NOT_FOUND, userId));
            throw new ApiException(String.format(ErrorMessage.USER_NOT_FOUND, userId), ExceptionType.USER_NOT_FOUND);
        }
        if (company.getBalance() < amount) {
            logger.error(ErrorMessage.BALANCE_NOT_ENOUGH);
            throw new ApiException(ErrorMessage.BALANCE_NOT_ENOUGH, ExceptionType.BAD_REQUEST);
        }
        List<Card> cards = user.getWallet().getCards();
        if (cards == null) {
            logger.error(ErrorMessage.CARD_NOT_FOUND);
            throw new ApiException(ErrorMessage.CARD_NOT_FOUND, ExceptionType.BAD_REQUEST);
        }
        Optional<Card> card = cards.stream().filter(card1 -> cardType.name().equals(card1.getType().name())).findFirst();
        if (!card.isPresent()) {
            logger.error(ErrorMessage.CARD_NOT_FOUND);
            throw new ApiException(ErrorMessage.CARD_NOT_FOUND, ExceptionType.BAD_REQUEST);
        }
        deposite(card.get(), amount, company);

    }

    @Override
    public UserBalanceResponse getUserBalance(Long userId) {
        // TODO get company id form context
        Long companyId = 1L;
        User user = userService.findUserById(userId).orElseThrow(() -> new ApiException(String.format(ErrorMessage.USER_NOT_FOUND, userId), ExceptionType.USER_NOT_FOUND));
        if (user.getCompany().getId() != companyId) {
            logger.error(String.format(ErrorMessage.USER_NOT_FOUND, userId));
            throw new ApiException(String.format(ErrorMessage.USER_NOT_FOUND, userId), ExceptionType.USER_NOT_FOUND);
        }
        List<Card> cards = user.getWallet().getCards();
        if (cards == null || cards.size() == 0) {
            logger.error(ErrorMessage.CARD_NOT_FOUND);
            throw new ApiException(ErrorMessage.CARD_NOT_FOUND, ExceptionType.BAD_REQUEST);
        }
        UserBalanceResponse response = UserBalanceResponse.builder().build();
        List<CardDto> cardDtos = new ArrayList<>();
        logger.trace("calcul user balance .. ");
        for (Card card : cards) {
            Float total = distributionRepository.getSumAllDepositDistributionNotExpired(card.getId(), DistributionType.DEPOSIT.name());
            cardDtos.add(CardDto.builder().cardBalance(card.getBalance()).cardId(card.getId()).cardType(card.getType()).notExpiredBalance(total).build());
        }
        response.setCards(cardDtos);
        return response;
    }

    private void deposite(Card card, Long amount, Company company) {
        logger.trace("update balance card");
        card.setBalance(card.getBalance() + amount);
        cardService.save(card);
        try {
            createDistribution(DistributionFactory.createDistribution(card, DistributionType.DEPOSIT, amount));
        } catch (Exception e) {
            logger.error(ErrorMessage.PROBLEM_TECHNIQUE + ": erreur when creating distribution - error message "+ e.getMessage());
            new ApiException(ErrorMessage.PROBLEM_TECHNIQUE, ExceptionType.SERVER_ERROR);
        }
        logger.trace("update balance company");
        company.setBalance(company.getBalance() - amount);
        companyService.save(company);
        logger.trace("distribution created");
    }

    private void createDistribution(Distribution distribution) {
        distributionRepository.save(distribution);
    }


}
