package com.wedogift.glady.Services;

import com.wedogift.glady.Model.Entities.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findUserById(Long userId);
    User save(User user);
}
