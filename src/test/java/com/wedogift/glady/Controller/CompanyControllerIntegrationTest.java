package com.wedogift.glady.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedogift.glady.GladyApplication;
import com.wedogift.glady.Model.Message.UserBalanceResponse;
import com.wedogift.glady.Repository.DistributionRepository;
import com.wedogift.glady.Services.DistributionService;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = GladyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")

public class CompanyControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    @Order(1)
    public void shouldDestributeMeal() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Object body = new Object() {
            public final Long userId = 1L;
            public final Long amount = 50L;
        };
        String json = objectMapper.writeValueAsString(body);
        mvc.perform(post("/company/distribute/meal").content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
    @Test
    @Order(2)
    public void shouldDistributeGift() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Object body = new Object() {
            public final Long userId = 1L;
            public final Long amount = 50L;
        };
        String json = objectMapper.writeValueAsString(body);
        mvc.perform(post("/company/distribute/gift").content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    @Order(3)
    public void shouldGetUserBalance() throws Exception {

       MvcResult result =  mvc.perform(get("/company/user/balance/{userId}",1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
       String s = "{\"cards\":[{\"cardId\":1,\"cardType\":\"GIFT\",\"cardBalance\":50,\"notExpiredBalance\":50.0},{\"cardId\":2,\"cardType\":\"MEAL\",\"cardBalance\":50,\"notExpiredBalance\":50.0}]}";
        assertThat(s).isEqualTo(result.getResponse().getContentAsString());
    }

}
