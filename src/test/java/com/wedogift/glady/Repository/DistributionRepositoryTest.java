package com.wedogift.glady.Repository;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Enum.DistributionType;
import com.wedogift.glady.Factory.CardFactory;
import com.wedogift.glady.Factory.DistributionFactory;
import com.wedogift.glady.Model.Entities.Card;
import com.wedogift.glady.Model.Entities.Distribution;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class DistributionRepositoryTest {

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private DistributionRepository distributionRepository;

    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private CardRepository cardRepository ;

    @Test
    public void should_find_no_distributions_if_repository_is_empty(){
        Iterable distributions = distributionRepository.findAll();
        assertThat(distributions).isEmpty();
    }
    @Test
    public void should_store_a_distribution() throws ParseException {
        Card card =  entityManager.find(Card.class,1L);
        Distribution distribution = distributionRepository.save(DistributionFactory.createDistribution(card, DistributionType.DEPOSIT,40L));
        assertThat(distribution).hasFieldOrPropertyWithValue("type", DistributionType.DEPOSIT);
        assertThat(formatter.format(distribution.getCreationDate())).isEqualTo(formatter.format(new Date()));
        assertThat(distribution.getCard().getId()).isEqualTo(1L);
        assertThat(formatter.format(distribution.getExpiredDate())).isEqualTo(formatter.format(DistributionFactory.getExpiredDateForGift()));
        assertThat(distribution.getType()).isEqualTo(DistributionType.DEPOSIT);
    }
    @Test
    public void should_get_sum_Deposit_distribution_not_expired() throws ParseException {
        Card card1 =  entityManager.find(Card.class,1L);
        Card card2 =  entityManager.find(Card.class,2L);
        Distribution d1 = DistributionFactory.createDistribution(card1,DistributionType.DEPOSIT,50L);
        Distribution d2 = DistributionFactory.createDistribution(card1,DistributionType.DEPOSIT,50L);
        Distribution d3 = DistributionFactory.createDistribution(card1,DistributionType.DEPOSIT,50L);
        Distribution d4 = DistributionFactory.createDistribution(card2,DistributionType.DEPOSIT,50L);
        d1.setExpiredDate(formatter.parse("2021-1-1"));
        entityManager.persist(d1);
        entityManager.persist(d2);
        entityManager.persist(d3);
        entityManager.persist(d4);

        Float total = distributionRepository.getSumAllDepositDistributionNotExpired(card1.getId(),DistributionType.DEPOSIT.name());
        assertThat(total).isEqualTo(100F);
    }
}
