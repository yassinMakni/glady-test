package com.wedogift.glady.Service;

import com.wedogift.glady.Enum.CardType;
import com.wedogift.glady.Model.Entities.Card;
import com.wedogift.glady.Model.Entities.Company;
import com.wedogift.glady.Model.Entities.User;
import com.wedogift.glady.Model.Entities.Wallet;
import com.wedogift.glady.Repository.DistributionRepository;
import com.wedogift.glady.Services.CardService;
import com.wedogift.glady.Services.CompanyService;
import com.wedogift.glady.Services.DistributionService;
import com.wedogift.glady.Services.Impl.DistributionServiceImpl;
import com.wedogift.glady.Services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DistributionServiceTest {

    @InjectMocks
    private DistributionServiceImpl distributionService ;

    @Mock
    private DistributionRepository distributionRepository ;

    @Mock
    private UserService userService;

    @Mock
    private CompanyService companyService;

    @Mock
    private CardService cardService ;



    @Test
    public void should_throw_Exception_if_user_not_found(){
        Mockito.when(userService.findUserById(1L)).thenReturn(Optional.empty());
        Assertions.assertThrows(Exception.class, () -> distributionService.distribute(1L,50L, CardType.GIFT));

    }
    //TODO
    public void should_throw_Exception_if_user_not_in_company(){

    }
    @Test
    public void should_throw_Exception_if_amount_is_not_valid(){
        Assertions.assertThrows(Exception.class, () -> distributionService.distribute(1L,-50L, CardType.GIFT));
    }
    //TODO
    public void should_throw_Exception_if_balance_not_enough(){

    }
    //TODO
    public void should_throw_exception_if_card_not_present(){

    }
    @Test
    public void should_distribute() throws Exception {
        Company company = Company.builder().balance(200L).id(1L).Name("Tesla").build() ;
        Card card = Card.builder().type(CardType.GIFT).id(1L).balance(0L).build();
        Mockito.when(userService.findUserById(1L)).thenReturn(Optional.of(User.builder().company(company).id(1L).wallet(Wallet.builder().cards(Arrays.asList(card)).build()).build()));
        Mockito.when(companyService.findCompanyById(1L)).thenReturn(Optional.of(company));
        Assertions.assertDoesNotThrow(()-> distributionService.distribute(1L,50L,CardType.GIFT));
    }
}
